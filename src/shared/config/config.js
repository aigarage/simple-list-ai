const config = {
  port: 8081,
  dbURL: "mongodb://localhost/landingpage",
  dbOptions: {},
  JWT_ACCESS_SECRET: "JWT",
};

export { config as default };
