import nodemailer from "nodemailer";
import dotenv from "dotenv";
dotenv.config();

const API_URL = process.env.API_URL;

class MailServiceClass {
  constructor() {
    const smtpConfig = {
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASSWORD,
      },
    };
    this.transporter = nodemailer.createTransport(smtpConfig);
    this.transporter.verify((err, success) => {
      if (err) {
        throw err;
      }
    });
  }

  async sendMagicLink(to, link, error) {
    try {
      await this.transporter.sendMail({
        from: process.env.SMTP_USER,
        to,
        subject: "Magic link to " + API_URL,
        html: `<!DOCTYPE html>
        <html lang="en">
        <head>
          <meta charset="UTF-8">
          <title>Email Activation Page</title>
          <style>
\            body {
              margin: 0;
              padding: 0;
              font-family: Arial, sans-serif;
              background-color: #f5f5f5;
              color: #444444;
              font-size: 16px;
              line-height: 1.5;
              text-align: center;
            }
            
            .container {
              max-width: 600px;
              margin: 0 auto;
              padding: 20px;
              background-color: #ffffff;
              box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1);
              border-radius: 5px;
              box-sizing: border-box;
              text-align: center;
            }
            
            h1 {
              font-size: 32px;
              font-weight: bold;
              margin-top: 0;
              margin-bottom: 20px;
            }
            
            p {
              margin-top: 0;
              margin-bottom: 20px;
            }
            
            a {
              display: inline-block;
              padding: 10px 20px;
              background-color: #4CAF50;
              color: #ffffff;
              text-decoration: none;
              border-radius: 5px;
              font-weight: bold;
              margin-top: 20px;
              transition: background-color 0.2s ease-in-out;
            }
            
            a:hover {
              background-color: #3e8e41;
            }
            
            @media screen and (max-width: 600px) {
              .container {
                padding: 10px;
              }
              
              h1 {
                font-size: 24px;
              }
              
              a {
                font-size: 14px;
                padding: 8px 16px;
              }
            }
          </style>
        </head>
        <body>
          <div class="container">
            <h1>Welcome to our service!</h1>
            <p>Thank you for signing up. Please activate your account by clicking the link below:</p>
            <a href="${API_URL}/auth/account?token=${link}">Activate Account</a>
          </div>
        </body>
        </html>
        `,
      }, error);
    } catch (e) {
      console.log(e);
    }
  }
}

export const MailService = new MailServiceClass();
