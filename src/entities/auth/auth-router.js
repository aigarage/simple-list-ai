import express from "express";
import { AuthController } from "./auth-controller.js";

export const AuthRouter = express.Router();

AuthRouter.post("/login", AuthController.login);

AuthRouter.get("/account", AuthController.isAuthenticated);