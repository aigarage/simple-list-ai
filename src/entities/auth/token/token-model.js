import mongoose from "mongoose";

const Schema = mongoose.Schema;

const TokenSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "user" },
});

export const TokenModel = mongoose.model("token", TokenSchema);
