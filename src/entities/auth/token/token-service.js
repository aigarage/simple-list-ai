import jwt from "jsonwebtoken";

export const TokenService = {
  generateTokens: (email) => {
    const expirationDate = new Date();
    expirationDate.setHours(new Date().getHours() + 1);
    return  jwt.sign(
      { email, expirationDate },
      process.env.JWT_ACCESS_SECRET
    );
  },
};
