import jwt from "jsonwebtoken";
import { MailService } from "../../shared/services/mail-service.js";
import { TokenService } from "./token/token-service.js";

export class AuthController {
  static async login(req, res, next) {
    try {
      console.log(req.body);
      const { email } = req.body;
      if (!email) {
        res.status(404);
        res.send({
          message: "You didn't enter a valid email address.",
        });
      }
      const token = TokenService.generateTokens(email);
      return MailService.sendMagicLink(email, token, (error) => {
        if (error) {
          res.status(404);
          res.send("Can't send email.");
        } else {
          res.status(200);
          res.send(
            `Magic link sent. : http://localhost:8081/auth/account?token=${token}`
          );
        }
      });
    } catch (e) {
      next(e);
    }
  }

  static async isAuthenticated(req, res, next) {
    try {
      const { token } = req.query;
      if (!token) {
        res.status(403);
        res.send("Can't verify user.");
        return;
      }
      let decoded;
      try {
        decoded = jwt.verify(token, process.env.JWT_ACCESS_SECRET);
      } catch {
        res.status(403);
        res.send("Invalid auth credentials.");
        return;
      }
      if (
        !decoded.hasOwnProperty("email") ||
        !decoded.hasOwnProperty("expirationDate")
      ) {
        res.status(403);
        res.send("Invalid auth credentials.");
        return;
      }
      const { expirationDate } = decoded;
      if (expirationDate < new Date()) {
        res.status(403);
        res.send("Token has expired.");
        return;
      }
      res.status(200);
      res.send("User has been validated.");
    } catch (e) {
      next(e);
    }
  }

}
